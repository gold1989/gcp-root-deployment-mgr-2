#!/usr/bin/env bash
set -e   # set -o errexit
set -u   # set -o nounset
set -o pipefail
[ "x${DEBUG:-}" = "x" ] || set -x

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

PROJECT_ID=
ROOT_ID=
SERVICE_ACCOUNT_NAME=sca-service-account
SERVICE_ACCOUNT_ID=cyb-sca
SERVICE_ACCOUNT_CONFIG_FILE=sca_service_account.yml
CUSTOM_SCA_ROLE_CONFIG_FILE=sca_custom_role_config.yml

function ShowUsage
{
    echo "Usage: $0 -p PROJECT_ID -r ROOT_ID"
}

while getopts p:r:d:hs o
do  case "$o" in
  p)  PROJECT_ID="$OPTARG";;
  r)  ROOT_ID="$OPTARG";;
  [?] | h) ShowUsage ; exit 1;;
  esac
done

if [[ "${PROJECT_ID}" == "PROJECT_ID" || "${ROOT_ID}" == "ROOT_ID" ]]; then
  ShowUsage
  exit 1
fi

if [[ -z "${PROJECT_ID}" || -z "${ROOT_ID}" ]]; then
  ShowUsage
  exit 1
fi

function PrintSrviceAccountError
{
cat << EOF
    ERROR: 'sca-service-account' deployment already exists.
    Please delete 'sca-service-account' deployment and all its resources and try again
EOF
}

function PrintCustomRoleError
{
cat << EOF
    ERROR: 'CustomSCARole' custom role already exists.
    Please delete 'CustomSCARole' custom role and try again.
EOF
}

function CheckExistingDeployment
{
    # Set cloud shell project project_id
    gcloud config set project ${PROJECT_ID} > /dev/null 2>&1

    for line in $(gcloud deployment-manager deployments list); do
        if [[ "${line}" == "sca-service-account" ]]; then
            PrintSrviceAccountError
            exit 1
        fi
    done
}

function RunDeploymentSteps
{
     # Create CustomSCARole and in case it exists update CustomSCARole
     gcloud iam roles create CustomSCARole --organization=${ROOT_ID} --file=${CUSTOM_SCA_ROLE_CONFIG_FILE} > /dev/null 2>&1 \
     || gcloud iam roles update CustomSCARole --organization=${ROOT_ID} --file=${CUSTOM_SCA_ROLE_CONFIG_FILE} --quiet

    # Create deployment
    echo "Creating SCA service account deployment..."
    gcloud deployment-manager deployments create ${SERVICE_ACCOUNT_NAME} \
    --config ${SERVICE_ACCOUNT_CONFIG_FILE}

    # Add service account to the root IAM
    echo "Adding SCA service account to organization and bind it to CustomeSCARole"
    gcloud organizations add-iam-policy-binding ${ROOT_ID} \
    --member=serviceAccount:${SERVICE_ACCOUNT_ID}@${PROJECT_ID}.iam.gserviceaccount.com \
    --role="organizations/${ROOT_ID}/roles/CustomSCARole" \
    --condition=None
}

CheckExistingDeployment
RunDeploymentSteps